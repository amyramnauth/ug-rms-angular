import { Component, OnInit } from '@angular/core';
import initWizard = require('../../../../assets/js/init/initWizard.js');

declare var $:any;
@Component({
    moduleId: module.id,
    selector: 'registration-cmp',
    templateUrl: 'registration.component.html'
})

export class RegistrationComponent implements OnInit{
    ngOnInit(){
        $.getScript('../../../assets/js/plugins/jquery.bootstrap-wizard.js');
        initWizard();
    }
}
