"use strict";
var registration_component_1 = require('./registration/registration.component');
//
exports.MODULE_ROUTES = [
    { path: '', redirectTo: 'register', pathMatch: 'full' },
    { path: 'register', component: registration_component_1.RegistrationComponent },
];
//
exports.MODULE_COMPONENTS = [
    registration_component_1.RegistrationComponent,
];
//# sourceMappingURL=product.routes.js.map