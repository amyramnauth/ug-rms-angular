import { Route, RouterModule } from '@angular/router';
import { RegistrationComponent } from './registration/registration.component';

//
export const MODULE_ROUTES: Route[] =[
    { path: '', redirectTo: 'register', pathMatch: 'full' },
    { path: 'register', component: RegistrationComponent },
]
//
export const MODULE_COMPONENTS = [
    RegistrationComponent,
]
